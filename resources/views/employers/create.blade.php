@extends('layouts.template');

@section('body')
<div class="container">

<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
            <div class="col-lg-5 d-none d-lg-block bg-register-image">
                <span>
                    Ajouter un nouvel employer

                </span>
            </div>
            <div class="col-lg-7">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">Ajouter employer</h1>
                    </div>
                    <form class="user" method="POST" action="{{ route('employers.store')}}">
                        @csrf
                        @method('POST')
                        <div class="form-group row">
                            <div class="col-sm-6">
                                  
                                  <select name="departement_id" id="departement_id" class=" form-control ">
                                    <option value="">Selectionner un departement</option>
                                    @foreach ($departements as $departement)
                                        <option value="{{$departement->id}}" class="">{{$departement->name}}</option>
                                    @endforeach
                                  </select>

                            </div>    
                            
                            <div class="col-sm-6">
                                <input type="number" class="form-control "id="exampleRepeatPassword" placeholder="Montant journalier" name="montant_journalier" value="{{old('montant_journalier')}}"><br>

                                @error('name')
                                    <div class="alert alert-danger form-control">{{ $message }}</div>   
                                @enderror
                           </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" class="form-control form-control-user" id="exampleFirstName" placeholder="Nom" name="name"><br>
                            
                                @error('name')
                                   <div class="alert alert-danger form-control">{{ $message }}</div>
                                @enderror
                            </div>           

                           
                            <div class="col-sm-6">
                                <input type="text" class="form-control form-control-user" id="exampleLastName" placeholder="Prenom" name="prenom"><br>
                           
                            @error('prenom')
                                <div class="alert alert-danger form-control">{{ $message }}</div>
                            @enderror
                            </div>
                        </div>


                       
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="email" class="form-control form-control-user"id="exampleInputPassword" placeholder="E-amil" name="email" value="{{old('email')}}"><br>
                            
                            @error('email')
                                <div class="alert alert-danger form-control">{{ $message }}</div><br>
                            @enderror
                            </div>

                            <div class="col-sm-6">
                                <input type="text" class="form-control form-control-user"id="exampleRepeatPassword" placeholder="Téléphone" name="contact" value="{{old('contact')}}"><br>
                            
                            @error('contact')
                                <div class="alert alert-danger form-control">{{ $message }}</div>
                            @enderror
                            </div>
                        </div>

        
                        <button type="submit" class="btn btn-primary btn-user btn-block">
                            Ajouter
                        </button>
                      
                        
                    </form>
                
                </div>
            </div>
        </div>
    </div>
</div>

</div>

@endsection