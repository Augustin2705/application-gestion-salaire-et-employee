@extends('layouts.template')

@section('body')

<div class="container-fluid">

                   
                  

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary"></h6>
                            
                            @if(Session::get('success'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('success') }}
                            </div>
                            @endif

                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Liste des employers</h1>

                        <a href="{{ route('employers.create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                                Ajouter Employer</a>
                          </div>

                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nom</th>
                                            <th>Prénom</th>
                                            <th>Email</th>
                                            <th>Contact</th>
                                            <th>Départements</th>
                                            <th>Salaire</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @forelse ($employers as $employer)

                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$employer->name}}</td>
                                                <td>{{$employer->prenom}}</td>
                                                <td>{{$employer->email}}</td>
                                                <td>{{$employer->contact}}</td>
                                                <td>{{$employer->departement->name}}</td>
                                                <td class=" btn bg-gradient-success" style="color:white">{{number_format($employer->montant_journalier * 31,2)}} Euros</td>
                                                <td>
                                                    <a href="{{ route('employers.edit', $employer->id) }}" class="btn bg-gradient-primary" style="color:white">Modifier</a>
                                                    <a href="{{ route('employers.delete', $employer->id) }}" class="btn btn-danger">Supprimer</a>
                                            </tr>

                                        @empty
                                            <tr>
                                                <td colspan="5">Aucun employer trouvé</td>
                                            </tr>

                                        
                                        @endforelse
                                      
                                    </tbody>
                                </table>
                            </div>
                            {{$employers->links()}}
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
        

@endsection