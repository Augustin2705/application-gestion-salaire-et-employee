@extends('layouts.template')

@section('body')

<div class="container-fluid">

                   
                  

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary"></h6>

                            @if(Session::get('success'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('success') }}
                            </div>
                            @endif

                            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Liste des départements</h1>

                        <a href="{{ route('departements.create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                                Ajouter Departement</a>
                          </div>

                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nom</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                               
                                    <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @forelse ($departements as $departement)

                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$departement->name}}</td>
  
                                                <td>
                                                    <a href="{{ route('departements.edit', $departement->id) }}" class="btn bg-gradient-primary" style="color:white">Modifier</a>
                                                    <a href="{{ route('departements.delete', $departement->id) }}" class="btn btn-danger">Supprimer</a>
                                                </td>
                                            </tr>

                                        @empty
                                            <tr>
                                                <td colspan="2">Aucun département trouvé</td>
                                            </tr>

                                        @endforelse
                                      
                                    </tbody>
                                </table>
                            </div>
                            {{$departements->links()}}

                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>


@endsection