@extends('layouts.template');

@section('body')
<div class="container">

<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
            <div class="col-lg-5 d-none d-lg-block bg-register-image">
                <span>
                    Mettre à jour une département

                </span>
            </div>
            <div class="col-lg-7">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">Modifier Département</h1>
                    </div>
                    <form class="user" method="POST" action="{{ route('departements.update', $departement->id) }}">
                    @csrf
                    @method('PUT')
                       
                        <div class="form-group">
                            <input name="name" id="departement_id" class="form-control form-control-user" placeholder="Nom du département" autocomplete="off" value="{{$departement->name}}">
                        </div>

                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <button type="submit" class="btn btn-primary btn-user btn-block">
                            Modifier
                        </button>
                      
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

@endsection