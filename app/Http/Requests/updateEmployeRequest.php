<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class updateEmployeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'departement_id' => 'required|integer',
            'name' => 'required|string',
            'prenom'=>'required|string',
            'email'=>'required',
            'contact'=>'required',
            'montant_journalier'=>'required'
        ];
    }

    public function message(){

        return [
        //lorsque le user n'a pas renseigner de nom 
        'name.required'=>'le nom est requis',
        'prenom.required'=>'le prenom est requis',
        'email.required'=>'l\'email est requis',
        'contact.required'=>'le contact est requis',
        'montant_journalier.required'=>'le montant journalier est requis',

    
 
        
    ];

    }
}
