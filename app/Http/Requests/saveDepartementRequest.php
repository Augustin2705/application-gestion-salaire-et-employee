<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class saveDepartementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            
            'name' => 'required|unique:departements,name'
        ];
    }

    public function message(){

        return [
        //lorsque le user n'a pas renseigner de nom 
        'name.required'=>'le nom est requis',
    
        //lorsque le user n'a pas renseigner un nom unique
        'name.unique'=>'le nom est deja pris',
    ];

    }
}
