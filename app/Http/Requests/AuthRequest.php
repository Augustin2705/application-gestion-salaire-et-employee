<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string',
        ];
    }

    public function message(){

        return [
        //lorsque le user n'a pas renseigner de mail 
        'email.required'=>'le mail est requis',
    
        //lorsque le user n'a pas renseigner un bon format mail
        'email.email'=>'le mail doit etre un mail valide',

        //lorsque le user n'a pas renseigner de mot de passe 
        'password.required'=>'le mot de passe est requis',
    ];

    }
}
