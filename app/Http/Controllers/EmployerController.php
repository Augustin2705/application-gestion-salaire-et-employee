<?php

namespace App\Http\Controllers;

use App\Models\Employer;
use App\Models\Departement;
use App\Http\Requests\storeEmployerequest;
use App\Http\Requests\updateEmployerequest;

use Illuminate\Http\Request;

class EmployerController extends Controller
{
    public function store(storeEmployerequest $request){

        $employer =Employer::create($request->all());

        $employer->save();
        return redirect()->route('employers.index')->with('success', "{$employer->name} a été bien ajouté");
    }

    public function index(){

        $employers=Employer::with('departement')->paginate(10);
        return view('employers.index', compact('employers'));

    }


    public function create(){
         
        $departements = Departement::all();
        return view('employers.create', compact('departements'));

    }


    public function edit(Employer $employer){

        $departements = Departement::all();
        return view('employers.edit', compact('employer', 'departements'));

    }

    public function update(updateEmployerequest $request, Employer $employer){

            $employer->name = $request->name;
            $employer->prenom = $request->prenom;
            $employer->email = $request->email;
            $employer->contact = $request->contact;
            $employer->montant_journalier = $request->montant_journalier;
            $employer->departement_id = $request->departement_id;
            $employer->update();
            return redirect()->route('employers.index')->with('success', "Modification apporté avec succès");

    }

    public function delete($id){
            
            $employer = Employer::find($id);
            $employer->delete();
            return redirect()->route('employers.index')->with('success', "{$employer->name} a été bien supprimé");
    }




}
