<?php

namespace App\Http\Controllers;

use App\Models\Departement;
use App\Models\Employer;
use App\Models\User;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        //avoir le nombre total département
        $totalDepartement = Departement::all()->count();

        //avoir le nombre total employé
        $totalEmploye = Employer::all()->count();

        //avoir le nombre total des adminns
        $totalAdministrateur = User::all()->count();
        return view('dashboard', compact('totalDepartement', 'totalEmploye', 'totalAdministrateur'));
    }
}
