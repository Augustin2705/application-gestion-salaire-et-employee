<?php

namespace App\Http\Controllers;

use App\Models\Departement;
use App\Http\Requests\saveDepartementRequest;
use Illuminate\Http\Request;

class DepartementController extends Controller
{
    public function store(saveDepartementRequest $request, Departement $departement){

        $departement = new Departement();
        $departement->name = $request->name;
        $departement->save();
        $departements = Departement::paginate();


        return redirect()->route('departements.index')->with('success', "{$departement->name} a été bien ajouté");
    }

    public function index(){

        $departements = Departement::paginate(3);
        return view('departements.index', compact('departements'));
    }


    public function create(){
        return view('departements.create');
    }


    public function edit(Departement $departement){

        return view('departements.edit', compact('departement'));
    }

    public function update(saveDepartementRequest $request, Departement $departement){

        $departement->name = $request->name;
        $departement->update();
        return redirect()->route('departements.index')->with('success', "Modification apporté avec succès");
    }

    public function delete($id){
        $departement = Departement::find($id);
        $departement->delete();
        return redirect()->route('departements.index')->with('success', "{$departement->name} a été bien supprimé");
    }


}
